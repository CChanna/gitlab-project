<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        // 注册 JPush 客户端单例
        $this->app->singleton(JPushClient::class, function ($app) {
            $options = [
                $app->config->get('jpush.key'),
                $app->config->get('jpush.secret'),
                $app->config->get('jpush.log'),
            ];

            return new JPushClient(...$options);
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
